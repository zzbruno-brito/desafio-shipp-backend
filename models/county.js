'use strict';
module.exports = (sequelize, DataTypes) => {
  const County = sequelize.define('County', {
    name: DataTypes.STRING
  }, {});
  County.associate = function(models) {
    // associations can be defined here
  };
  return County;
};