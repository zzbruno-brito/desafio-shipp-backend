'use strict';
module.exports = (sequelize, DataTypes) => {
  const Store = sequelize.define('Store', {
    licenseNumber: DataTypes.STRING,
    entityName: DataTypes.STRING,
    operationType: DataTypes.STRING,
    dbaName: DataTypes.STRING,
    countyId: DataTypes.INTEGER,
    establishmentTypeId: DataTypes.INTEGER
  }, {});
  Store.associate = function(models) {
    Store.belongsTo(models.EstablishmentType);
    Store.belongsTo(models.County);
    Store.hasMany(models.Address);
  };
  return Store;
};