'use strict';
module.exports = (sequelize, DataTypes) => {
  const Address = sequelize.define('Address', {
    streetName: DataTypes.STRING,
    streetNumber: DataTypes.STRING,
    line2: DataTypes.STRING,
    line3: DataTypes.STRING,
    zipCode: DataTypes.STRING,
    city: DataTypes.STRING,
    state: DataTypes.STRING,
    latitude: DataTypes.DOUBLE,
    longitude: DataTypes.DOUBLE,
    squareFootage: DataTypes.DOUBLE,
    needsRecoding: DataTypes.BOOLEAN,
    storeId: DataTypes.INTEGER,
  }, {});
  Address.associate = function(models) {
    Address.belongsTo(models.Store)
  };
  return Address;
};