'use strict';
module.exports = (sequelize, DataTypes) => {
  const EstablishmentType = sequelize.define('EstablishmentType', {
    name: DataTypes.STRING
  }, {});
  EstablishmentType.associate = function(models) {
    // associations can be defined here
  };
  return EstablishmentType;
};