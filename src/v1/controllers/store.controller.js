const StoreService = require('../services/store.services');

const StoreCtrl = {};

let wrap = fn => (req, res, next) => fn(req, res, next).catch(next);

StoreCtrl.list = wrap(async (req, res) => {
  const { latitude, longitude } = req.query;
  try {
    let stores = await StoreService.listNearest(latitude, longitude);
    res.send(stores);
  } catch (e) {
    throw e;
  }
});

module.exports = StoreCtrl;
