const { Store, Address, County, EstablishmentType } = require('../../../models');
const StoreService = {};

function getDistance(lat1, lon1, lat2, lon2, unit) {
  if ((lat1 === lat2) && (lon1 === lon2)) {
    return 0;
  }
  else {
    let radlat1 = Math.PI * lat1/180;
    let radlat2 = Math.PI * lat2/180;
    let theta = lon1-lon2;
    let radtheta = Math.PI * theta/180;
    let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1) {
      dist = 1;
    }
    dist = Math.acos(dist);
    dist = dist * 180/Math.PI;
    dist = dist * 60 * 1.1515;
    if (unit==="K") { dist = dist * 1.609344 }
    if (unit==="N") { dist = dist * 0.8684 }
    return dist;
  }
}

StoreService.listNearest = async function (lat, lng) {
  let stores = await Store.findAll({include: [{model: Address, required: true}, {model: County}, {model: EstablishmentType}]});
  let storeDistances = stores.map(store => {
    let address = store.Addresses[0];

    return {
      id: store.id,
      licenseNumber: store.licenseNumber,
      entityName: store.entityName,
      operationType: store.operationType,
      dbaName: store.dbaName,
      countyId: store.countyId,
      establishmentTypeId: store.establishmentTypeId,
      createdAt: store.createdAt,
      updatedAt: store.updatedAt,
      establishmentType: store.EstablishmentType.name,
      county: store.County.name,
      lat: address.latitude,
      lng: address.longitude,
      distance: getDistance(lat, lng, address.latitude, address.longitude, 'K')
    }
  });

  let sortedStores = storeDistances.sort((a, b) => a.distance - b.distance);
  let filteredStores = sortedStores.filter(store => store.distance <= 6.5 && store.lat !== null);
  return filteredStores.slice(0, 6);
};

module.exports = StoreService;