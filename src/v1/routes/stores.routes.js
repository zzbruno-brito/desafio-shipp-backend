'use strict';
const mung = require('express-mung');
const express = require('express');
const router = new express.Router();
const StoreCtrl = require('../controllers/store.controller');
const fs = require('fs');

function createLog(dir, filename, data) {
    if (!fs.existsSync(filename)){
        fs.mkdirSync(dir);
        fs.writeFileSync(filename, data);
    }
    else
        fs.appendFileSync(filename, data);

}

function logger(body, req, res) {
    const { latitude, longitude } = req.query;
    res.on('finish', function () {
        const statusCode = res.statusCode;
        const totalStores = body.length ? body.length : 0;
        const date = new Date().toISOString();
        let data = `${date} - latitude: ${latitude} - longitude: ${longitude} - status code: ${statusCode} - total stores: ${totalStores}\n`;
        createLog('logs', 'logs/response.log', data);
    });
}

router.use(mung.json(logger, {mungError: true}));

function validateParams(req, res, next) {
    const { latitude, longitude } = req.query;

    if (!latitude || !longitude) res.status(400).send({error: true, message: 'Os parâmetros latitude e longitude são obrigatórios'});
    else next();
}


router.get('/', validateParams, StoreCtrl.list);

module.exports = router;
