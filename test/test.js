//Require the dev-dependencies

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index');
let should = chai.should();


chai.use(chaiHttp);

describe('Stores', () => {

    describe('/GET stores', () => {
        it('it should GET all the nearest stores', (done) => {
            chai.request(server)
                .get('/v1/stores?latitude=-72.142351&longitude=-72.142351')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.lessThan(7);
                    done();
                });
        });

        it('it should NOT GET all the nearest stores', (done) => {
            chai.request(server)
                .get('/v1/stores?latitude=-72.142351&longitude=')
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });
    });



});