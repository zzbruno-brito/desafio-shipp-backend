const express = require('express');
const http = require('http');
const app = express();
const models = require("./models");

app.set('port', process.env.PORT || 3001);

app.use('/v1/stores', require('./src/v1/routes/stores.routes'));

models.sequelize.sync().then(function () {
    server.listen(app.get('port'));
    server.on('error', onError);
    server.on('listening', onListening);
});

const server = http.createServer(app);

function onError(error) {
    console.log(error);
}

function onListening() {
    console.log('Server started on port ' + app.get('port'));
}

module.exports = app;