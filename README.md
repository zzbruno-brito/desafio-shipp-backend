Use o seguinte comando para preparar e executar a aplicação:
`$ npm run start-application`

Pronto! A aplicação pode ser usada.

A porta usada é a 3001.

Os arquivos de log estão na pasta logs/


O comando acima apenas executa todos os comandos abaixo, caso queixa executá-los separadamente:

Para instalar as dependências:

`$ npm install`

Gerar as estruturas do banco de dados:

`$ npm run migrate`

Importar os dados do .csv

`$ npm run import-data`

Executar os testes:

`$ npm test`

Executar o projeto:

`$ node index.js`
