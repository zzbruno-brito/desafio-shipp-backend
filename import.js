const { County, EstablishmentType, Store, Address } = require('./models');
const fs = require('fs');
const csv = require('csv-parser');

async function getData(path) {
    let data = [];

    return new Promise((resolve, reject) => {
        fs.createReadStream(path)
            .pipe(csv())
            .on('data', (row) => {
                data.push(row);
            })
            .on('end', () => {
                console.log('CSV file successfully processed');
                resolve(data);
            });
    });
}

function filterCounties(counties) {
    let countyNames = [...new Set(counties.map(county => county.County))] ;
    return countyNames.map((name, id) => {
        return { id:id+1, name }
    })
}

function filterEstablishments(establishments) {
    let establishmentNames = [...new Set(establishments.map(establishment => establishment['Establishment Type']))] ;
    return establishmentNames.map((name, id) => {
        return { id:id+1, name }
    })
}

function getStoresAndLocations(data, counties, establishments) {
    return data.map((row, index) => {
        let county = counties.find(county => county.name === row.County);
        let establishment = establishments.find(establish => establish.name === row['Establishment Type']);
        let storeId = index+1;

        // não foi possível usar JSON.parse para converter a string em objeto, então optei por capturar os valores usando regex
        let rawLocation = row.Location;
        let longitudeRe = rawLocation.match(/longitude':\s+?'(.+?)'/);
        let latitudeRe = rawLocation.match(/latitude':\s+?'(.+?)'/);
        let needsRecodingRe = rawLocation.match(/needs_recoding':\s(.+?),/);

        let address = {
            storeId,
            streetName: row['Street Name'].trim(),
            streetNumber: row['Street Number'],
            line2: row['Address Line 2'].trim(),
            line3: row['Address Line 2'].trim(),
            zipCode: row['Zip Code'],
            city: row.City,
            state: row.State,
            squareFootage: parseFloat(row['Square Footage'])
        };

        if (longitudeRe && longitudeRe.length > 1)
            address.longitude = parseFloat(longitudeRe[1]);

        if (latitudeRe && latitudeRe.length > 1)
            address.latitude = parseFloat(longitudeRe[1]);

        if (needsRecodingRe && needsRecodingRe.length > 1){
            address.needsRecoding = needsRecodingRe[1] !== 'False';
        }

        return {
            store: {
                id: storeId,
                licenseNumber: row['License Number'],
                entityName: row['Entity Name'].trim(),
                operationType: row['Operation Type'],
                dbaName: row['DBA Name'].trim(),
                countyId: county.id,
                establishmentTypeId: establishment.id
            },
            address: address

        }
    })
}



(async () => {

    let data = await getData('./stores.csv');
    let uniqueCounties = filterCounties(data);
    let uniqueEstablishments = filterEstablishments(data);
    let result = getStoresAndLocations(data, uniqueCounties, uniqueEstablishments);
    let stores = result.map(item => item.store);
    let addresses = result.map(item => item.address);

    try {
        await County.bulkCreate(uniqueCounties);
        await EstablishmentType.bulkCreate(uniqueEstablishments);
        await Store.bulkCreate(stores);
        await Address.bulkCreate(addresses);
    } catch (e) {
        console.log(e);
    }



})();