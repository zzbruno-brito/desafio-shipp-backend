'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.addColumn(
        'stores',
        'countyId',
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'counties',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        });
  },

  down: (queryInterface, Sequelize) => {

    return queryInterface.removeColumn(
        'stores',
        'countyId'
    );
  }
};
