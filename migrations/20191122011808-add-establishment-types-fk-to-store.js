'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.addColumn(
        'stores',
        'establishmentTypeId',
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'EstablishmentTypes',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        });
  },

  down: (queryInterface, Sequelize) => {

    return queryInterface.removeColumn(
        'stores',
        'establishmentTypeId'
    );
  }
};
